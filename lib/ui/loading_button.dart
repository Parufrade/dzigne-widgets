library loading_button;

// LOADINGBUTTON
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// LoadingButton Widget
class LoadingButton extends StatefulWidget {
  // Ponteiro da função onTap
  final Future<bool> Function() onTap;
  final Future<Null> Function(bool success) onDone;
  final Color color;
  final String text;

  LoadingButton(
      {Key key,
      @required this.onTap,
      @required this.color,
      this.onDone,
      this.text})
      : super(key: key);

  @override
  _LoadingButtonState createState() => new _LoadingButtonState();
}

// LoadingButtonState
class _LoadingButtonState extends State<LoadingButton>
    with TickerProviderStateMixin {
  // Controles das animações do bobtão
  AnimationController _btnSizeAnimationController, _btnZoomAnimationController;
  CurvedAnimation btnSizeAnimation, btnZoomAnimation;

  // Propriedades que variam com as animações
  double buttonWidth;
  double buttonHeight = 36.0;

  // Resultado da operação
  bool hasSucceeded = false;

  // Conteúdo interno do botão de avançar, varia de acordo com a animação
  Widget get buttonContent {
    // Animação da largura
    if (this.btnZoomAnimation.value > 0) {
      // Mostra animação do resultado
      return this.success;
    } else {
      // Largura pequena, mostra o loading ao invés do texto
      if (this.btnSizeAnimation.value > 0.7) {
        return this.loading;
      } else {
        // Largura normal, mostra o texto ao invés do loading
        return this.text;
      }
    }
  }

  // Está acessando o servidor?
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    // Animações do botão: SIZE
    this._btnSizeAnimationController = new AnimationController(
      duration: const Duration(milliseconds: 400),
      vsync: this,
    );

    this.btnSizeAnimation = new CurvedAnimation(
      parent: this._btnSizeAnimationController,
      curve: Curves.easeInOut,
    );

    this.btnSizeAnimation.addListener(() {
      setState(() {});
    });

    // Animações do botão: ZOOM
    this._btnZoomAnimationController = new AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    );

    this.btnZoomAnimation = new CurvedAnimation(
      parent: this._btnZoomAnimationController,
      curve: Curves.bounceOut,
      reverseCurve: Curves.easeInOut,
    );

    this.btnZoomAnimation.addListener(() {
      setState(() {});
    });
  }

  // Handle o toque no botão animando-o e chamando o ponteiro informado
  Future<Null> onBtnTap() async {
    if (!this._isLoading) {
      HapticFeedback.selectionClick();

      this._isLoading = true;

      // Anima a largura do botão
      await this._btnSizeAnimationController.forward();

      if (widget.onTap != null) {
        // Chama o método ponteiro que gerencia o toque
        this.hasSucceeded = await widget.onTap();
      }

      // Deu certo?
      if (this.hasSucceeded) {
        HapticFeedback.lightImpact();

        // Sim, anima a mensagem de sucesso
        if (this.mounted) {
          await this._btnZoomAnimationController.forward();
          await Future.delayed(Duration(milliseconds: 3000));
        }

        //this._btnSizeAnimationController.reverse();
        // await this._btnZoomAnimationController.reverse();
      } else {
        await this._btnSizeAnimationController.reverse();
      }

      this._isLoading = false;

      if (widget.onDone != null) {
        widget.onDone(this.hasSucceeded);
      }
    }
  }

  @override
  void dispose() {
    this._btnSizeAnimationController.dispose();
    this._btnZoomAnimationController.dispose();
    super.dispose();
  }

  // Atualiza as propriedades com os valores das animações
  void resetProperties() {
    // Largure
    this.buttonWidth =
        (180.0 - this.buttonHeight) * (1.0 - this.btnSizeAnimation.value) +
            this.buttonHeight;
  }

  @override
  Widget build(BuildContext context) {
    // Atualiza as propriedades calculando as animações
    this.resetProperties();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            GestureDetector(
              onTap: this.onBtnTap, // responde ao toque no botão
              child: Container(
                alignment: FractionalOffset.center,
                width: this.buttonWidth, // Animação da largura
                height: this.buttonHeight, // Animação da altura
                decoration: BoxDecoration(
                  color: widget.color,
                  borderRadius: BorderRadius.all(
                    Radius.circular(
                        this.buttonHeight / 2.0), // Animação da borda do botão
                  ),
                ),
                child: this
                    .buttonContent, // conteúdo interno do botão de acordo com o estado
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget get loading {
    return SizedBox(
      width: 22.0,
      height: 22.0,
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(
          Colors.white,
        ),
      ),
    );
  }

  Widget get text {
    return Text(
      widget.text == null ? 'Save' : widget.text,
      style: TextStyle(color: Colors.white),
    );
  }

  Widget get success {
    return Icon(
      Icons.check,
      color: Colors.white,
      size: this.buttonHeight * this.btnZoomAnimation.value,
    );
  }
}
