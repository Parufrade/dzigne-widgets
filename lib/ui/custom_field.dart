library custom_field;

import 'package:flutter/material.dart';
import 'package:masked_text/masked_text.dart';

class CustomField {
  // Campos de texto
  static Widget textInput({
    String label,
    TextEditingController controller,
    TextInputType keyboardType = TextInputType.text,
    int maxLength,
    bool autoCorrect = false,
    bool readOnly = false,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return TextField(
      keyboardType: keyboardType,
      maxLength: maxLength,
      controller: controller,
      readOnly: readOnly,
      autocorrect: autoCorrect,
      style: TextStyle(
        fontSize: 14.0,
      ),
      decoration: InputDecoration(
        labelText: label,
        counterText: "",
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
      ),
    );
  }

  // Campos de texto com formatação
  static Widget masktextInput({
    String label,
    String mask,
    int maxLength,
    TextInputType keyboardType = TextInputType.number,
    TextEditingController controller,
    Widget prefixIcon,
    Widget suffixIcon,
  }) {
    return MaskedTextField(
      keyboardType: keyboardType,
      mask: mask,
      maxLength: maxLength,
      maskedTextFieldController: controller,
      inputDecoration: InputDecoration(
        labelText: label,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        labelStyle: TextStyle(
          fontSize: 14.0,
        ),
        counterText: "",
      ),
    );
  }

  // Dropdown
  static Widget dropDown(
      {String label,
      List<String> options,
      String content,
      void Function(String newValue) callback}) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8.0,
        bottom: 8.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          DropdownButton<String>(
            style: TextStyle(
              fontSize: 14.0,
            ),
            hint: Text(label),
            isExpanded: true,
            value: content == "" ? null : content,
            onChanged: (String newValue) => callback(newValue),
            items: options.map((String item) {
              return DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: TextStyle(color: Colors.black),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
