import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MultiSelectionButtons extends StatefulWidget {
  final Function(List<int> selectedIndexes, List<String> selectedOptions) onTap;
  final List<String> options;
  final bool scrolls;
  final List<int> selectedIndexes;
  final bool wrap;

  MultiSelectionButtons({
    this.onTap,
    @required this.options,
    this.scrolls,
    this.selectedIndexes,
    this.wrap,
  });

  @override
  _MultiSelectionButtonsState createState() => _MultiSelectionButtonsState();
}

class _MultiSelectionButtonsState extends State<MultiSelectionButtons> {
  bool scrolls = false;
  double itemExtent = 0;
  GlobalKey containerKey = GlobalKey();
  bool wrap = false;

  List<int> selectedIndexes;
  List<String> selectedOptions;

  @override
  void initState() {
    this.selectedIndexes = new List<int>();
    this.selectedOptions = new List<String>();
    if (widget.selectedIndexes != null) {
      widget.selectedIndexes.forEach((item) {
        this.selectedIndexes.add(item);
        this.selectedOptions.add(widget.options[item]);
      });
    }

    this.wrap = widget.wrap == null ? false : widget.wrap;

    this.scrolls = widget.scrolls != null ? widget.scrolls : false;
    if (!this.scrolls) {
      WidgetsBinding.instance.addPostFrameCallback(afterLayout);
    }
    super.initState();
  }

  // Called after laying out widgets
  void afterLayout(_) {
    setState(() {
      this.itemExtent =
          this.containerKey.currentContext.size.width / widget.options.length;
    });
  }

  // Button selected
  void onBtnSelected(int index, String option) {
    HapticFeedback.selectionClick();
    if (this.selectedIndexes.contains(index)) {
      this.selectedIndexes.remove(index);
      this.selectedOptions.remove(option);
    } else {
      this.selectedIndexes.add(index);
      this.selectedOptions.add(option);
    }
    if (widget.onTap != null) {
      widget.onTap(this.selectedIndexes, this.selectedOptions);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 2.0,
        bottom: 2.0,
      ),
      child: this.wrap
          ? Container(
              key: this.containerKey,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Wrap(
                      direction: Axis.horizontal,
                      spacing: 8.0,
                      alignment: WrapAlignment.center,
                      children: widget.options.map((item) {
                        int index = widget.options.indexOf(item);
                        return FlatButton(
                          highlightColor: Color(0x80bfe0dd),
                          splashColor: Color(0xFFbfe0dd),
                          color: !this.selectedIndexes.contains(index)
                              ? Color(0xFFbfe0dd).withOpacity(0.2)
                              : Color(0xFFbfe0dd),
                          onPressed: () =>
                              this.onBtnSelected(index, widget.options[index]),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                              60.0,
                            ),
                          ),
                          child: Text(
                            widget.options[index],
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Color(0xFF1f7799),
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
            )
          : Container(
              key: this.containerKey,
              height: 48.0,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  this.scrolls
                      ? Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Color(0x801f7799),
                            size: 12.0,
                          ),
                        )
                      : Container(
                          width: 0.0,
                          height: 0.0,
                        ),
                  Expanded(
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemExtent: this.itemExtent == 0 ? null : this.itemExtent,
                      shrinkWrap: true,
                      itemCount: widget.options.length,
                      itemBuilder: (BuildContext context, int index) {
                        return FlatButton(
                          highlightColor: Color(0x80bfe0dd),
                          splashColor: Color(0xFFbfe0dd),
                          color: !this.selectedIndexes.contains(index)
                              ? Colors.transparent
                              : Color(0xFFbfe0dd),
                          onPressed: () =>
                              this.onBtnSelected(index, widget.options[index]),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                              60.0,
                            ),
                          ),
                          child: Text(
                            widget.options[index],
                            style: TextStyle(
                              fontSize: 16.0,
                              color: Color(0xFF1f7799),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  this.scrolls
                      ? Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Color(0x801f7799),
                            size: 12.0,
                          ),
                        )
                      : Container(
                          width: 0.0,
                          height: 0.0,
                        ),
                ],
              ),
            ),
    );
  }
}
