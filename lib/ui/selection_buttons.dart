import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SelectionButtons extends StatefulWidget {
  final Function(int index, String text) onTap;
  final List<String> options;
  final bool scrolls;
  final int selectedIndex;

  SelectionButtons({this.onTap, @required this.options, this.scrolls, this.selectedIndex,});

  @override
  _BHKButtonsState createState() => _BHKButtonsState();
}

class _BHKButtonsState extends State<SelectionButtons> {
  int selectedIndex = -1;
  bool scrolls = false;
  double itemExtent = 0;
  GlobalKey containerKey = GlobalKey();

  @override
  void initState() {
    if (widget.selectedIndex!=null) {
      this.selectedIndex = widget.selectedIndex;
    }
    this.scrolls = widget.scrolls!=null ? widget.scrolls : false;
    if (!this.scrolls) {
      WidgetsBinding.instance.addPostFrameCallback(afterLayout);
    }
    super.initState();
  }

  // Called after laying out widgets
  void afterLayout(_) {
    setState(() {
      this.itemExtent = this.containerKey.currentContext.size.width / widget.options.length; 
    });
  }

  // Button selected
  void onBtnSelected(int index) {
    HapticFeedback.selectionClick();
    setState(() {
      this.selectedIndex = index;
    });
    if (widget.onTap!=null) {
      widget.onTap(this.selectedIndex, widget.options[this.selectedIndex]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0,),
      child: Container(
        key: this.containerKey,
        height: 48.0,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            this.scrolls ? Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                Icons.arrow_back_ios,
                color: Color(0x801f7799),
                size: 12.0,
              ),
            ) : Container(width: 0.0, height: 0.0,),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemExtent: this.itemExtent == 0 ? null : this.itemExtent,
                shrinkWrap: true,
                itemCount: widget.options.length,
                itemBuilder: (BuildContext context, int index) {
                  return FlatButton(
                      highlightColor: Color(0x80bfe0dd),
                      splashColor: Color(0xFFbfe0dd),
                      color: this.selectedIndex != index
                          ? Colors.transparent
                          : Color(0xFFbfe0dd),
                      onPressed: () => this.onBtnSelected(index),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                          60.0,
                        ),
                      ),
                      child: Text(
                        widget.options[index],
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Color(0xFF1f7799),
                        ),
                      ),
                  );
                },
              ),
            ),
            this.scrolls ? Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Color(0x801f7799),
                size: 12.0,
              ),
            ) : Container(width: 0.0, height: 0.0,),
          ],
        ),
      ),
    );
  }
}
