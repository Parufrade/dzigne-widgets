import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// Types of dropdownfield
class DropdownListType {
  static DropdownListType get titleBeginSearch {
    return new DropdownListType(0);
  }

  static DropdownListType get titleAndSubtitleBeginSearch {
    return new DropdownListType(1);
  }

  static DropdownListType get titleSearch {
    return new DropdownListType(2);
  }

  static DropdownListType get titleAndSubtitleSearch {
    return new DropdownListType(3);
  }

  static DropdownListType get optionsList {
    return new DropdownListType(4);
  }

  int type = 0;
  DropdownListType(int type) {
    this.type = type;
  }
}

class DropdownList extends StatefulWidget {
  final TextEditingController controller;
  final String label;
  final List<DropdownListItem> items;
  final Function(DropdownListItem item, int index) onSelectionChanged;
  final String subject;
  final DropdownListType type;
  final Function() onAdd;
  final Widget prefixIcon;
  final TextInputType keyboardType;
  final bool autoCorrect;

  DropdownList({
    this.controller,
    this.label = "",
    @required this.items,
    @required this.onSelectionChanged,
    this.subject,
    this.type,
    this.onAdd,
    this.prefixIcon,
    this.keyboardType,
    this.autoCorrect,
  });
  @override
  _DropdownFieldState createState() => _DropdownFieldState();
}

class _DropdownFieldState extends State<DropdownList> {
  double maxHeight = 240.0;
  int trigger;
  double _listHeight = 0.0;
  List<DropdownListItem> filteredList;
  TextEditingController localController;
  String _subject;
  TextInputType _keyboardType;
  bool _autoCorrect;

  @override
  void initState() {
    //this.maxHeight = widget.items.length > 10 ? 240.0 : 140.0;
    this._keyboardType = widget.keyboardType ?? TextInputType.text;
    this._autoCorrect = widget.autoCorrect ?? true;
    this.filteredList = new List<DropdownListItem>();
    this.localController = widget.controller == null
        ? new TextEditingController()
        : widget.controller;
    this._subject = widget.subject == null ? "items" : widget.subject;

    // Check the type of list and select the trigger (number of characters to activate the list)
    this.trigger = widget.type != null && widget.type.type > 1
        ? widget.type.type > 3 ? 0 : 3
        : 1;

    // If list is type 4 (Options) then fill it
    if (widget.type != null && widget.type.type == 4) {
      widget.items.forEach((item) {
        this.filteredList.add(item);
      });
    }
    super.initState();
  }

  // Receive changes on textedit field
  void _onChange(String text) {
    // Toogle list according to text length and the trigger
    if (this._listHeight == 0.0 && text.length >= this.trigger) {
      this._toggleList();
    } else if (this._listHeight > 0.0 && text.length < this.trigger) {
      this._toggleList();
      widget.onSelectionChanged(null, 0);
    }

    // Filter list according to its type
    if (text.length >= this.trigger) {
      if (widget.type != null) {
        switch (widget.type.type) {
          case 0:
            this.filteredList.clear();
            widget.items.forEach((item) {
              if (item.title.toLowerCase().startsWith(text.toLowerCase())) {
                this.filteredList.add(item);
              }
            });
            break;
          case 1:
            this.filteredList.clear();
            widget.items.forEach((item) {
              if (item.title.toLowerCase().startsWith(text.toLowerCase()) ||
                  item.subTitle.toLowerCase().startsWith(text.toLowerCase())) {
                this.filteredList.add(item);
              }
            });
            break;
          case 2:
            this.filteredList.clear();
            widget.items.forEach((item) {
              if (item.title.toLowerCase().contains(text.toLowerCase())) {
                this.filteredList.add(item);
              }
            });
            break;
          case 3:
            this.filteredList.clear();
            widget.items.forEach((item) {
              if (item.title.toLowerCase().contains(text.toLowerCase()) ||
                  item.subTitle.toLowerCase().contains(text.toLowerCase())) {
                this.filteredList.add(item);
              }
            });
        }
      } else {
        widget.items.forEach((item) {
          if (item.title.toLowerCase().startsWith(text.toLowerCase())) {
            this.filteredList.add(item);
          }
        });
      }

      setState(() {
        this._listHeight = this._calcListHeight();
      });
    }
  }

  // Shows or hides the list
  void _toggleList() {
    HapticFeedback.selectionClick();
    if (this._listHeight == 0.0) {
      setState(() {
        this._listHeight = this._calcListHeight();
      });
    } else {
      setState(() {
        this._listHeight = 0.0;
      });
    }
  }

  // Calculates listheight
  double _calcListHeight() {
    double height = 36 * (filteredList.length.toDouble() + 1);
    return this.filteredList.length == 0
        ? 48.0
        : height > this.maxHeight ? this.maxHeight : height;
  }

  // Item selected on list
  void _onItemSelected(DropdownListItem item, int index) {
    HapticFeedback.selectionClick();
    this._toggleList();
    this.localController.text = item.title;
    FocusScope.of(context).requestFocus(FocusNode());
    widget.onSelectionChanged(item, index);
  }

  // Check if textfield has been tapped and shows or hides the list
  void _onTextFieldTap() {
    if (this._listHeight != 0.0) {
      this._toggleList();
    } else if (widget.type != null && widget.type.type == 4) {
      this._toggleList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: TextField(
                onChanged: (text) => this._onChange(text),
                onTap: this._onTextFieldTap,
                readOnly: (widget.type?.type ?? 0) == 4,
                controller: this.localController,
                style: TextStyle(
                  fontSize: 14.0,
                ),
                keyboardType: this._keyboardType,
                autocorrect: this._autoCorrect,
                decoration: InputDecoration(
                  prefixIcon: widget.prefixIcon,
                  suffixIcon: widget.type != null && widget.type.type == 4
                      ? Icon(Icons.arrow_drop_down)
                      : Icon(Icons.search),
                  labelText: widget.label,
                  counterText: "",
                ),
              ),
            ),
            widget.onAdd != null
                ? IconButton(
                    icon: Icon(Icons.add_circle_outline, color: Colors.grey,),
                    onPressed: widget.onAdd,
                  )
                : Container(
                    width: 0.0,
                    height: 0.0,
                  ),
          ],
        ),
        AnimatedContainer(
          height: this._listHeight,
          child: Column(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: this.filteredList.length > 0
                      ? ListView.builder(
                          itemCount: this.filteredList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return itemTile(index);
                          },
                        )
                      : Padding(
                          padding: EdgeInsets.only(top: 4.0),
                          child: Text(
                            "No ${this._subject} found",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14.0,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                        ),
                ),
              ),
              this._listHeight > 0
                  ? Divider(
                      color: Colors.grey,
                    )
                  : Container(
                      width: 0.0,
                      height: 0.0,
                    ),
            ],
          ),
          duration: Duration(milliseconds: 500),
          curve: Curves.easeOutCirc,
        ),
      ],
    );
  }

  Widget itemTile(int index) {
    return SizedBox(
      height: 36.0,
      child: FlatButton(
        highlightColor: Color(0x80bfe0dd),
        splashColor: Color(0xFFbfe0dd),
        padding: EdgeInsets.only(
          left: 8.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(this.filteredList[index].title,
                style: TextStyle(
                  fontSize: 14.0,
                )),
            this.filteredList[index].subTitle != null
                ? Text(this.filteredList[index].subTitle,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.grey,
                    ))
                : Container(
                    width: 0.0,
                    height: 0.0,
                  ),
          ],
        ),
        onPressed: () => this._onItemSelected(this.filteredList[index], index),
      ),
    );
  }
}

class DropdownListItem {
  final int id;
  final String title;
  final String subTitle;

  DropdownListItem({
    this.id = 0,
    @required this.title,
    this.subTitle,
  });
}
