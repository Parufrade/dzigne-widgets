
import 'dart:io';

class Server {
  // Download file
  static Future<File> downloadFile(String url, String localFileName) async {
    HttpClient client = new HttpClient();
    File file = new File(localFileName);
    var request = await client.getUrl(Uri.parse(url));
    var response = await request.close();
    await response.pipe(file.openWrite());
    return file;
  }
}