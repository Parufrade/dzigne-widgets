// SISTEMA REALTORCONNECT
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

// Classe RestAPI: comunica com a API no servidor
class RestAPI {
  static const int TRIES = 4;
  static const int INTERVAL = 2; // Seconds between tries

  // Endereco da API remota
  String baseUrl = "";
  String token = "";

  // Construtor
  RestAPI(this.baseUrl, this.token);

  // Operação GET
  Future get(String url) async {
    String encodedURL = Uri.encodeFull("${this.baseUrl}/$url");
    encodedURL = encodedURL.replaceAll("@", "%40"); // Email

    var jsonDecoded;
    int t = RestAPI.TRIES - 1; // Not keep trying

    print(encodedURL);

    while (jsonDecoded == null && t < RestAPI.TRIES) {
      try {
        t++;
        final response = await http.get(
          encodedURL,
          headers: {
            "Accept": "applications/json",
            "UserToken": token,
          },
        );

        if (response.statusCode == 200) {
          jsonDecoded = json.decode(response.body);
          print("Resultado: $jsonDecoded");
        } else {
          print(
              "ERRO: " + response.statusCode.toString() + " " + response.body);
        }
      } catch (e) {
        print(e.toString());
      }

      if (jsonDecoded == null) {
        print('Server connection error. Trying again $t...');
        await Future.delayed(Duration(seconds: RestAPI.INTERVAL));
      }
    }

    return jsonDecoded;
  }

  // Operação POST
  Future post(String url, Map<String, String> data) async {
    var jsonDecoded;
    int t = 0;

    print("API POST: $url ...");
    while (jsonDecoded == null && t < RestAPI.TRIES) {
      try {
        t++;
        final response = await http.post(
          Uri.encodeFull("${this.baseUrl}/$url"),
          body: data,
          headers: {
            "Accept": "applications/json",
            "UserToken": this.token,
          },
        );

        print("Response body: " + response.body);

        if (response.statusCode == 200) {
          jsonDecoded = json.decode(response.body);

          print("Resultado: $jsonDecoded");
        } else {
          print(
              "ERRO: " + response.statusCode.toString() + " " + response.body);
        }
      } catch (e) {
        print(e.toString());
      }
      if (jsonDecoded == null) {
        print('Server connection error. Trying again $t...');
        await Future.delayed(Duration(seconds: RestAPI.INTERVAL));
      }
    }
    return jsonDecoded;
  }

  // Upload Image
  Future uploadImage(String url, String fileName, File image, int id) async {
    var jsonDecoded;
    int t = 0;

    print("API POST uploading image: $url ...");

    while (jsonDecoded == null && t < RestAPI.TRIES) {
      try {
        t++;
        final response = await http.post(
          Uri.encodeFull("${this.baseUrl}/$url"),
          body: {
            'id': id.toString(),
            'filename': fileName,
            'image': image != null
                ? 'data:image/png;base64,' +
                    base64Encode(image.readAsBytesSync())
                : '',
          },
          headers: {
            "Accept": "applications/json",
            "UserToken": this.token,
          },
        );

        if (response.statusCode == 200) {
          jsonDecoded = json.decode(response.body);

          print("Resultado: $jsonDecoded");
        } else {
          print(
              "ERRO: " + response.statusCode.toString() + " " + response.body);
        }
      } catch (e) {
        print(e.toString());
      }
      if (jsonDecoded == null) {
        print('Server connection error. Trying again $t...');
        await Future.delayed(Duration(seconds: RestAPI.INTERVAL));
      }
    }
    return jsonDecoded;
  }
}
