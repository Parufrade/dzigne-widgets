// Store
// Copyright Dzign-e Sistemas e Technologia Ltda
import 'package:dzigne/db/local_database.dart';
import 'package:dzigne/net/restapi.dart';

abstract class StoreDB<T> {
  RestAPI api;
  List<T> rows;
  final String tableName;

  bool _isLoading;

  bool get isLoading {
    return this._isLoading;
  }

  StoreDB({
    this.tableName,
    String baseUrl,
    String token,
  }) {
    this.api = new RestAPI(
      baseUrl,
      token,
    );
    this.rows = new List<T>();
    this._isLoading = false;
  }

  void add(row) => this.rows.add(row);

  // Fetch data from server
  Future fetchFromServer(String resource, {String parameters}) async {
    print("Fetching: $resource...");
    this._isLoading = true;
    parameters = parameters == null ? "" : "?$parameters";
    // Reads data from server
    var result = await this.api.get(resource + parameters);
    this._isLoading = false;
    return result;
  }

  // Load from DB
  Future<List<Map<String, dynamic>>> queryFromCache(String query) async {
    return await LocalDatabase.instance.query(query);
  }

  // Load from DB
  Future<List<Map<String, dynamic>>> selectFromCache({
    String where,
    List<dynamic> whereArgs,
    String orderBy,
    int limit,
    int offSet,
  }) async {
    return await LocalDatabase.instance.select(this.tableName,
        where: where,
        whereArgs: whereArgs,
        orderBy: orderBy,
        limit: limit,
        offSet: offSet);
  }

  // Saves all data on local storage
  Future<bool> saveToCache(String tableName, List<dynamic> rows) async {
    try {
      for (int t = 0; t < rows.length; t++) {
        await LocalDatabase.instance.save(tableName, rows[t]);
      }
    } catch (e) {
      print("StoreDB:" + e.toString());
      return false;
    }
    return true;
  }

  // Fetch from server and update local db
  Future synchronizeTable() async {
    String lastUpdate = await this.getLastUpdate(); // Get the latest record
    lastUpdate = lastUpdate != null
        ? "lastupdate=$lastUpdate"
        : null; // Get the updatedAt field
    // fetch updated records from server
    var list = await this
        .fetchFromServer(this.tableName, parameters: lastUpdate) as List;
    if (list != null) {
      if (await this.saveToCache(this.tableName, list)) {
        print("${list.length} ${this.tableName} synchronized.");
      }
    }
  }

  // Get updatedAt field from lates record on local database
  Future<String> getLastUpdate();

  // Get from local database
  Future<List<T>> get({
    String where,
    List<dynamic> whereArgs,
    String orderBy,
    int limit,
    int offSet,
  });
}
