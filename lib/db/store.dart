// Store
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'dart:convert';

import 'package:dzigne/net/restapi.dart';
import 'package:dzigne/io/local_storage.dart';

class Store<T> {
  RestAPI api;
  List<T> rows;

  bool _isLoading;

  bool get isLoading {
    return this._isLoading;
  }

  Store(String baseUrl, String token) {
    this.api = new RestAPI(
      baseUrl,
      token,
    );
    this.rows = new List<T>();
    this._isLoading = false;
  }

  void add(row) => this.rows.add(row);

  // Fetch data from server and save the data in local file
  Future fetch(String resource, String cacheName) async {
    print("Fetching: $resource...");

    this._isLoading = true;

    // Reads data from server
    var result = await this.api.get(resource);

    // If data from server is not available
    if (result == null) {
      // Reads from local storage is cacheName is defined
      if (cacheName != null) {
        result = await this.loadFromCache(cacheName, "");
      }
    } else if (cacheName != null) {
      // Saves data to local storage
      await this.toCache(cacheName, result);
    }

    this._isLoading = false;

    return result;
  }

  // Load from local storage
  Future loadFromCache(
    String cacheName,
    String folder,
  ) async {
    String jsonStr;
    var result;

    LocalStorage cache = new LocalStorage(
      fileName: cacheName,
      folder: folder,
    );
    jsonStr = await cache.load();
    print(jsonStr);
    if (jsonStr != null) {
      result = json.decode(jsonStr);
    }

    return result;
  }

  // This method should be overriden
  Future<List<T>> get(bool fromCache) async {
    return null;
  }

  // Try to fetch from local storage and update from server
  Future fetchAndUpdate() async {
    await this.get(true);
    if (this.rows.length == 0) {
      await this.get(false);
    } else {
      this.get(false);
    }
  }

  // Saves all data on local storage
  Future<bool> toCache(String localCacheName, var data) async {
    LocalStorage cache = new LocalStorage(
      fileName: localCacheName,
    );
    await cache.save(json.encode(data));
    return true;
  }

  Future deleteCache(String localCacheName) async {
    LocalStorage cache = new LocalStorage(
      fileName: localCacheName,
    );
    cache.delete();
  }
}
