// Model
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'dart:async';
import 'package:dzigne/db/local_database.dart';
import 'package:dzigne/net/restapi.dart';

class ModelDB<T> {
  RestAPI api;

  final String tableName;
  int id;

  bool _isLoading;

  Map<String, String> toJson() => null;

  bool get isLoading {
    return this._isLoading;
  }

  bool get isSaving {
    return this._isLoading;
  }

  ModelDB({this.tableName, String baseUrl, String token,}) {
    this.api = new RestAPI(baseUrl, token);
    this._isLoading = false;
  }

  // Saves on the server 
  Future post(String resource, Map<String, String> json) async {
    this._isLoading = true;
    var result = await this.api.post(resource, json);
    this._isLoading = false;
    return result;
  }

  // Save local
  Future<int> saveToCache(String tableName, Map<String, dynamic> json) async {
    return await LocalDatabase.instance.save(tableName, json);
  }

  // Save this item on remote DB
  Future<bool> save() async {
    var result = await this.post(this.tableName, this.toJson());
    this.id = result != null ? int.parse(result['id']) : 0;
    return result != null;
  }
}
