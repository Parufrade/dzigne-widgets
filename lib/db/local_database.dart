import 'dart:io';
import 'package:dzigne/db/store_db.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

// Singleton
class LocalDatabase {
  static LocalDatabase _instance;
  static String databaseFileName = "defaultDB.db";
  static Database _database;
  static int version = 1;
  static String createStatement = "";
  static String updateStatement = "";

  static int _syncIndex = 0;
  static int _syncTotalTables = 0;

  static LocalDatabase get instance {
    _instance = _instance == null ? new LocalDatabase() : _instance;
    return _instance;
  }

  Future<Database> get database async {
    if (_database != null && _database.isOpen) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // Open the database
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, databaseFileName);
    print("Starting local database: $path ...");
    return await openDatabase(
      path,
      version: version,
      onCreate: _onCreate,
      onUpgrade: _onUpdate,
    );
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    print("Creating local database: ${db.path} ...");
    List<String> statements = createStatement.split(";");
    statements.forEach((statement) async {
      await db.execute(statement);
    });
  }

  // SQL string to update the database
  Future _onUpdate(Database db, int oldVersion, int newVersion) async {
    print("Updating  local database: ${db.path} ...");
    List<String> statements = updateStatement.split(";");
    statements.forEach((statement) async {
      await db.execute(statement);
    });
  }

  // Insert
  Future<int> save(String table, Map<String, dynamic> json) async {
    Database db = await database;
    var row = await db.query(table,
        columns: ['id'], where: 'id=?', whereArgs: [json['id']]);
    int id = 0;
    if (row.length > 0) {
      id = await db.update(table, json, where: 'id=?', whereArgs: [json['id']]);
    } else {
      id = await db.insert(table, json);
    }

    return id;
  }

  // Select
  Future<List<Map<String, dynamic>>> select(
    String table, {
    List<String> fields,
    String where,
    List<dynamic> whereArgs,
    String orderBy,
    int limit,
    int offSet,
  }) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      table,
      columns: fields,
      where: where,
      whereArgs: whereArgs,
      limit: limit,
      offset: offSet,
      orderBy: orderBy,
    );
    return maps;
  }

  // Query
  Future<List<Map<String, dynamic>>> query(String query) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.rawQuery(query);
    return maps;
  }

  // Delete
  Future<int> delete(
    String table, {
    String where,
    List<dynamic> whereArgs,
  }) async {
    Database db = await database;
    return await db.delete(
      table,
      where: where,
      whereArgs: whereArgs,
    );
  }

  // Update
  Future<int> update(
    String table,
    Map<String, dynamic> values,
    String where,
    List<dynamic> whereArgs,
  ) async {
    Database db = await database;
    return await db.update(
      table,
      values,
      where: where,
      whereArgs: whereArgs,
    );
  }

  // Checkif database exists
  Future<bool> dbExists() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, databaseFileName);
    return await databaseExists(path);
  }

  // Deleted the database
  Future dbDelete() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, databaseFileName);
    if (await this.dbExists()) {
      print('Deleting database $path ...');
      await _database.close();
      await deleteDatabase(path);
    }
  }

  // Synchronize tables
  Future synchronizeDatabase(
      {List<StoreDB> tables,
      Function(String table, int index, int total) step}) async {
    if (tables != null && tables.length > 0) {
      _syncTotalTables = tables.length;
      _syncIndex = 0;
      for (int t = 0; t < tables.length; t++) {
        print("Synchronizing ${tables[t].tableName}...");
        if (step != null) {
          step(tables[t].tableName, _syncIndex, _syncTotalTables);
        }
        await tables[t].synchronizeTable();
        _syncIndex++;
      }
      if (step != null) {
        step('', _syncIndex, _syncTotalTables);
      }
    }
  }

  // Synchronize tables
  Future synchronizeDatabaseAsync(
      {List<StoreDB> tables,
      Function(String table, int index, int total) step}) async {
    if (tables != null && tables.length > 0) {
      _syncTotalTables = tables.length;
      _syncIndex = 0;
      if (step != null) {
            step(tables.first.tableName, _syncIndex, _syncTotalTables);
          }
      for (int t = 0; t < tables.length; t++) {
        print("Synchronizing ${tables[t].tableName}...");
        tables[t].synchronizeTable().then((_) {
          _syncIndex++;
          if (step != null) {
            step(tables[t].tableName, _syncIndex, _syncTotalTables);
          }
        });
      }
      while (_syncIndex < _syncTotalTables) {
        await Future.delayed(Duration(milliseconds: 100));
      }
      if (step != null) {
        step('', _syncIndex, _syncTotalTables);
      }
    }
  }
}
