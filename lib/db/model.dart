// Model
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'dart:async';
import 'dart:convert';

import 'package:dzigne/net/restapi.dart';
import 'package:dzigne/io/local_storage.dart';

class Model {
  RestAPI api;

  bool _isLoading;
  int id;

  bool get isLoading {
    return this._isLoading;
  }

  bool get isSaving {
    return this._isLoading;
  }

  Model(String baseUrl, String token) {
    this.api = new RestAPI(baseUrl, token);
    this._isLoading = false;
  }

  // Salva no servidor e local se cachename for <> null
  Future post(
      String resource, Map<String, String> json, String cacheName) async {
    this._isLoading = true;
    var result = await this.api.post(resource, json);
    if (result != null && cacheName != null) {
      // Salva local
      await this.toCache(cacheName, "", result);
    }
    this._isLoading = false;
    return result;
  }

  // Salva dados no cache local
  Future<bool> toCache(String localCacheName, String folder, var data) async {
    LocalStorage cache = new LocalStorage(
      fileName: localCacheName,
      folder: folder,
    );
    var jsonStr = json.encode(data);
    await cache.save(jsonStr);
    return true;
  }
}
