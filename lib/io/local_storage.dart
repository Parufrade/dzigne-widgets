// Local Storage
// Copyright Dzign-e Sistemas e Technologia Ltda

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class LocalStorage {
  final String fileName;
  final String folder;

  LocalStorage({this.fileName, this.folder});

  Future<File> get _jsonLocalFile async {
    if (this.fileName != null) {
      final path = await _localPath;
      final extension = this.fileName.endsWith(".json") ? "" : ".json";
      return File('$path/${this.fileName}$extension');
    } else {
      return null;
    }
  }

  Future<File> get localFile async {
    if (this.fileName != null) {
      final path = await _localPath;
      return File('$path/${this.fileName}');
    } else {
      return null;
    }
  }

  // Lista os arquivos salvos no dispositivo
  Future<List<FileSystemEntity>> dir() async {
    final directory = new Directory(await this._localPath);
    List<FileSystemEntity> files;

    if (directory.existsSync()) {
      files = directory.listSync(recursive: true, followLinks: false);
    }

    return files;
  }

  // Path local
  Future<String> get _localPath async {
    var directory = await getApplicationDocumentsDirectory();
    var path = directory.path;
    if (this.folder != null && this.folder != "") {
      path = "$path/${this.folder}";
    }
    return path;
  }

  // Salva arquivo
  Future<File> save(String contents) async {
    // Verifica se o diretório existe
    var dir = new Directory(await this._localPath);
    if (!dir.existsSync()) {
      dir.createSync(
        recursive: true,
      );
    }

    final file = await this._jsonLocalFile;
    if (file != null) {
      return file.writeAsString(contents);
    } else {
      return null;
    }
  }

  // Lê dados do arquivo
  Future<String> load() async {
    try {
      final file = await _jsonLocalFile;
      String contents;
      // Lê o arquivo
      if (file != null) {
        contents = await file.readAsString();
      }
      return contents;
    } catch (e) {
      print("Cache not available!");
      return null;
    }
  }

  // Apaga arquivo
  Future<bool> delete() async {
    try {
      final file = await _jsonLocalFile;

      // Apaga o arquivo
      if (file != null) {
        debugPrint('Deleting file: ${file.path}');
        file.deleteSync();
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  // Copia arquivo
  Future<bool> copyFromFile(File file) async {
    final path = await _localPath;
    return file.copySync('$path/${this.fileName}') != null;
  }
}
