# Dzign-e Widgets

This package provides some useful widgets for app development.

## Getting Started

This project is a starting point for a Dart
[package](https://flutter.dev/developing-packages/),
a library module containing code that can be shared easily across
multiple Flutter or Dart projects.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

## Installation

Just add the following lines on pubspec.yaml:
```
  http: ">=0.13.0"
  masked_text: ">=0.0.6"
  path_provider: ">=1.3.0"
  sqflite: ^1.3.0+2
  dzigne:
    git:
      url: git@gitlab.com:Parufrade/dzigne-widgets.git
```

## Widgets Provided

Database:
- `Model` - handles a database entity and provides syncronization between server and local storage.
- `Store` - handles a list of database entities and provides syncronization between server and local storage.
- `ModelDB` - handles a database entity and provides local storage on SQLite locally.
- `StoreDB` - handles a list of database entities and provides storage on SQLite locally.
- `LocalDataBase` - handles the SQLite database.

IO:
- `LocalStorage` - handles local storage.

Net:
- `RestAPI`- provides communication to a REST API on remote server.
- `Server` - provides static method to download files.

UI:
- `CustomField` - provides custom input fields.
- `DropdownList` - provides a dropdown input fiels with search and add features.
- `LoadingButton` - provides a nice loading button for server or any other asyncronous tasks.
- `SelectionButtons` - provides a nice horizontal list of buttons to be selected.
- `MultiSelectionButtons` - provides a nice horizontal list of buttons to be selected. This is a multiselection version.